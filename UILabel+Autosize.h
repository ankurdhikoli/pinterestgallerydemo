//
//  UILabel+Autosize.h
//  PinterestDemo
//
//  Created by Ankur Chauhan on 04/10/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Autosize)

- (void) autosizeForWidth: (int) width;


@end
