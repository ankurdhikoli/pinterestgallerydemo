//
//  UILabel+Autosize.m
//  PinterestDemo
//
//  Created by Ankur Chauhan on 04/10/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "UILabel+Autosize.h"

@implementation UILabel (Autosize)

- (void) autosizeForWidth: (int) width {
    self.lineBreakMode = NSLineBreakByWordWrapping;
    self.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(width, FLT_MAX);
    CGSize expectedLabelSize = [self.text sizeWithFont:self.font constrainedToSize:maximumLabelSize lineBreakMode:self.lineBreakMode];
    CGRect newFrame = self.frame;
    newFrame.size.height = expectedLabelSize.height;
    self.frame = newFrame;
}


@end
